# covid_restservice

This project is part of the Software Engineering course
in the Master Automation Management program at the FHNW.

## Java version
Java 15.0.1 is used in the project.

## Build instruction
Maven: 

## Dependencies
Dependencies are managed with Maven.

## Running
java -jar DataLoader_PROD_1_0.jar app.properties


