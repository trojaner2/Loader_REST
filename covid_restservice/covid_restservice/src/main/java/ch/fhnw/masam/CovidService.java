package ch.fhnw.masam;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CovidService {

	@Autowired
    private CovidRepository covidRepository;
    
    public List<CovidCase> findByDate(String date) {
    	return covidRepository.findByDate(date);
    }
    
    public List<CovidCase> findByCountry(String country) {
    	return covidRepository.findByCountry(country);
    }
    
    public List<String> getAllCountries() {
    	return covidRepository.getAllCountries();
    }
	
}
